#! /usr/bin/python

import argparse


def iswinner(board):
    for i in range(3):
        if board[i] and len(set(board[i::3])) == 1:
            print('case1')
            return board[i]
        if board[i * 3] and len(set(board[i * 3:(i * 3) + 3])) == 1:
            print('case2')
            return board[i * 3]
    if board[0] and len(set(board[0::4])) == 1:
        print('case3')
        return board[0]
    if board[2] and len(set(board[2:7:2])) == 1:
        print('case4')
        return board[2]
    return False


def display(board):
    dboard = [x if x else ' ' for x in board]
    print("    1   2   3")
    print("a |", " | ".join(dboard[0:3]), "|")
    print("b |", " | ".join(dboard[3:6]), "|")
    print("c |", " | ".join(dboard[6:]), "|")

def play(board, turn):
    while True:
        entry = input('Player ' + turn + ' turn :')
        if (
                len(entry) == 2
                and entry[0] in ['1', '2', '3']
                and entry[1] in ['a', 'b', 'c']
            ):
            position = int(entry[0]) - 1 + (3 * (ord(entry[1]) - ord('a')))
            if not board[position]:
                board[position] = turn
                return board
            else:
                print('invalid position, already taken')
        else:
            print('invalid entry, 2 chars expected, column number and line letter (ex: 1a)')


def main():
    board = 9 * ['']
    winner = False
    turn = 'X'
    while not winner:
        display(board)
        board = play(board, turn)
        turn = 'O' if turn == 'X' else 'X'
        winner = iswinner(board)
    display(board)
    print('Winner is', winner)

if __name__ == "__main__":
    main()