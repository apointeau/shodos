import argparse

# https://www.sudokuweb.org/fr/

def process(fd):
    content = fd.read()
    content = content.replace('\n', '')
    print(content)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filepath", nargs='+')
    args = parser.parse_args()
    for fpath in args.filepath:
        with open(fpath, 'r') as fd:
            process(fd)

if __name__ == "__main__":
    main()